#include "isketch.h"
#include "client.h"
#include "args.h"
#include "socket.h"
#include "protocole.h"
#include "variable.h"
#include "parse.h"
#include "str.h"

iSketch::iSketch()
{
    setupUi(this);

    char* tmp = (char*) malloc(sizeof(char)*BUF_SIZE);
    memset(tmp, 0, BUF_SIZE);

    strcat(tmp, "iSketch | ");
    strcat(tmp, pseudo);

    this->setWindowTitle(tmp);

    Parse *lt = new Parse();
    QObject::connect(lt, SIGNAL(emitAccessDenied()), this, SLOT(accessdenied()));
    QObject::connect(lt, SIGNAL(emitGame(char*)), this, SLOT(addGame(char*)));
    QObject::connect(lt, SIGNAL(emitChat(char*)), this, SLOT(addChat(char*)));
    QObject::connect(lt, SIGNAL(emitRoleMot(char*,char*)), this, SLOT(addRoleMot(char*,char*)));
    QObject::connect(lt, SIGNAL(emitLine(int,int,int,int,int,int,int,int)), this, SLOT(addLine(int,int,int,int,int,int,int,int)));
    QObject::connect(lt, SIGNAL(emitCourbe(int,int,int,int,int,int,int,int,int,int,int,int)), this, SLOT(addCourbe(int,int,int,int,int,int,int,int,int,int,int,int)));
    lt->start();
}







// Main
int main(int argc, char *argv[])
{
    gestionArg(argc, argv);
    QApplication app(argc, argv);

    debut();

    iSketch fenetre;
    fenetre.show();

    return app.exec();
}






// Signaux
void iSketch::accessdenied() {
    printf("ACCESS DENIED\n");
    this->close();
}

void iSketch::addGame(char *s) {
    gameText->append(s);
}

void iSketch::addChat(char *s) {
    chatText->append(s);
}

void iSketch::addRoleMot(char *r, char *m) {
    if(spectator) {
        roleLabel->setText("Spectateur");
        if(strcmp(r, "Dessinateur") == 0) {
            motLabel->setText(m);
        }
        gameButton->hide();
        inputGame->hide();
        cheatButton->hide();
        passButton->hide();
    }
    else {
        roleLabel->setText(r);
        motLabel->setText(m);
        if(dessinateur) {
            gameButton->hide();
            inputGame->hide();
            cheatButton->hide();
            passButton->show();

            psize(sock, 1);
            sizeNumber->display(1);

            pcolor(sock, 0, 0, 0);
        }
        else {
            gameButton->show();
            inputGame->show();
            cheatButton->show();
            passButton->hide();
        }
    }

    p1.setX(0);
    p1.setY(0);
    p2.setX(0);
    p2.setY(0);

    nbPts = 0;
    v_lines.clear();
    v_pen.clear();
    v_path.clear();
    v_pen_path.clear();

    update();
}

void iSketch::addLine(int x1, int y1, int x2, int y2, int r , int g , int b, int size) {
    QPen pen;
    QColor color = QColor(r, g, b, 255);

    pen.setWidth(size);
    pen.setStyle(Qt::SolidLine);
    pen.setBrush(color);
    pen.setCapStyle(Qt::RoundCap);
    pen.setJoinStyle(Qt::RoundJoin);

    v_pen << pen;

    QPalette palette;
    palette.setColor(QPalette::ButtonText, color);
    colorButton->setPalette(palette);

    sizeNumber->display(size);
    v_lines << QLine(x1, y1, x2, y2);
    update();
}

void iSketch::addCourbe(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4,int r , int g , int b, int size) {  
    if(x1 < 638 && x1 > 142 && x2 < 638 && x2 > 142 && x3 < 638 && x3 > 142 && x4 < 638 && x4 > 142)
        if(y1 < 478 && y1 > 232 && y2 < 478 && y2 > 232 && y3 < 478 && y3 > 232 && y4 < 478 && y4 > 232) {
            QPen pen;
            QColor color = QColor(r, g, b, 255);

            pen.setWidth(size);
            pen.setStyle(Qt::SolidLine);
            pen.setBrush(color);
            pen.setCapStyle(Qt::RoundCap);
            pen.setJoinStyle(Qt::RoundJoin);

            v_pen_path << pen;

            QPalette palette;
            palette.setColor(QPalette::ButtonText, color);
            colorButton->setPalette(palette);

            sizeNumber->display(size);

            QPainterPath path;
            path.moveTo(x1, y1);
            path.cubicTo(x2, y2, x3, y3, x4, y4);
            v_path << path;

            update();
        }
}






// Evenement souris

bool iSketch::eventFilter(QObject *obj, QEvent *qevent)
{
    if(dessinateur) {
        //vérifie que le QObject est une QFrame
        //et que le type d'evenement est "bouton de la souris relâché"
        QFrame * f = qobject_cast<QFrame *>(obj);
        if(f && qevent->type() == QEvent::MouseButtonRelease) {
            //on sait deja que c'est un QMouseEvent, donc un static_cast suffit
            QMouseEvent *mevent = static_cast<QMouseEvent *> (qevent);
            if(!courbeBox->isChecked())
                if(p1.x()< 638 && p1.x() > 142)
                    if(p1.y() < 478 && p1.y() > 232) {
                        p2 = mevent->pos();
                        pline(sock, p1.x(), p1.y(), p2.x(), p2.y());
                        update();
                    }
        }
        //on appelle la fonction du parent
        return QWidget::eventFilter(obj,qevent);
    }
    else
        return false;
}


// ajout du point cliqué lors de l'événement "bouton de la souris appuyé".
void iSketch::mousePressEvent(QMouseEvent *e)
{
    if(dessinateur) {
        if(e->x() < 638 && e->x() > 142)
            if(e->y() < 478 && e->y() > 232) {
                if(courbeBox->isChecked())
                    switch (nbPts) {
                    case 0:
                        p1 = e->pos();
                        nbPts++;
                        break;
                    case 1:
                        p2 = e->pos();
                        nbPts++;
                        break;
                    case 2:
                        p3 = e->pos();
                        nbPts++;
                        break;
                    case 3:
                        p4 = e->pos();
                        nbPts = 0;
                        pcourbe(sock, p1.x(), p1.y(), p2.x(), p2.y(), p3.x(), p3.y(), p4.x(), p4.y());
                        update();
                        break;
                    default:
                        break;
                    }
                else {
                    nbPts = 0;
                    p1 = e->pos();
                }
            }
            else
                nbPts = 0;
        else
            nbPts = 0;
    }
}

// ajout du point cliqué lors de l'événement "bouton de la souris relâché".
void iSketch::mouseReleaseEvent(QMouseEvent *e)
{
    if(dessinateur)
        if(!courbeBox->isChecked())
            if(p1.x()< 638 && p1.x() > 142)
                if(p1.y() < 478 && p1.y() > 232)
                    if(e->x() < 638 && e->x() > 142)
                        if(e->y() < 478 && e->y() > 232) {
                            p2 = e->pos();
                            pline(sock, p1.x(), p1.y(), p2.x(), p2.y());
                            update();
                        }
}





// paint
void iSketch::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);


    p.fillRect(142, 232, 496, 246, Qt::white);


    for(int i =0;i<v_path.size();i++) {
        p.setPen(v_pen_path.value(i));
        p.drawPath(v_path.value(i));
    }

    for(int i =0;i<v_lines.size();i++) {
        p.setPen(v_pen.value(i));
        p.drawLine(v_lines.value(i));
    }
}















// Appuie sur envoie chat
void iSketch::on_chatButton_accepted()
{
    QString message = inputChat->text();
    if(message.length()>0) {
        ptalk(sock, qPrintable(message));

        inputChat->clear(); // On vide la zone d'écriture du message
        inputChat->setFocus(); // Et on remet le curseur à l'intérieur
    }
}

// Appuie sur envoie game
void iSketch::on_gameButton_accepted()
{
    QString message = inputGame->text();
    if(message.length()>0) {
        pguess(sock, qPrintable(message));

        inputGame->clear(); // On vide la zone d'écriture du message
        inputGame->setFocus(); // Et on remet le curseur à l'intérieur
    }
}

// Appuie sur pass
void iSketch::on_passButton_clicked()
{
    ppass(sock);
}

// Appuie sur cheat
void iSketch::on_cheatButton_clicked()
{
    pcheat(sock, pseudo);
}


// Deplacement du slider size
void iSketch::on_sizeSlider_valueChanged(int n) {
    if(dessinateur) {
        psize(sock, n);
        sizeNumber->display(n);
    }
}

// Fenetre pour choisir la couleur
void iSketch::on_colorButton_clicked() {
    if(dessinateur) {
        QColor couleur = QColorDialog::getColor(Qt::white, this);

        QPalette palette;
        palette.setColor(QPalette::ButtonText, couleur);
        colorButton->setPalette(palette);
        pcolor(sock, couleur.red(), couleur.green(), couleur.blue());
    }
}


void iSketch::closeEvent(QCloseEvent *) {
    finir();
}
