#ifndef SOCKET_H
#define SOCKET_H

#include "client.h"


//void app();
void debut();
void finir();

/* Gestion socket */
int init_connection();
void end_connection(int sock);
int read_server(SOCKET sock, char *buffer);
/* Fin Gestion socket */

/* Thread de lecture */
//void *lecture(void *sock);

#endif /* guard */
