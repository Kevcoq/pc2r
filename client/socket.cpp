#include "client.h"
#include "variable.h"
#include "protocole.h"



/* Gestion Socket */
// Initialisation de la connexion
int init_connection() {
    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    SOCKADDR_IN sin = { 0 };
    struct hostent *hostinfo;

    if(sock == INVALID_SOCKET) {
        perror("socket()");
        exit(errno);
    }

    hostinfo = gethostbyname(host);
    if (hostinfo == NULL) {
        fprintf (stderr, "Unknown host %s.\n", ADDRESS);
        exit(EXIT_FAILURE);
    }

    sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
    sin.sin_port = htons(PORT);
    sin.sin_family = AF_INET;

    if(connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR) {
        perror("connect()");
        exit(errno);
    }
    return sock;
}

// Fermeture de la connexion
void end_connection(int sock) {
    closesocket(sock);
}


// Lecture dans la socket
int read_server(SOCKET sock, char *msg) {
    memset(msg, 0, BUF_SIZE);
    int n;
    if((n = read(sock, msg, BUF_SIZE - 1)) < 0) {
        perror("erreur dans read_server");
        exit(errno);
    }
    msg[n] = 0;
    return n;
}
/* FIN Gestion Socket */





/* Thread * Ancien thread avant passage a qt
// Thread de lecture
void *lecture(void *sock) {
  char *msg = (char*) malloc(sizeof(char)*BUF_SIZE);
  char msg2[BUF_SIZE];
  int n;
  while((n = read_server((SOCKET)sock, msg))) {
    memset(msg2, 0, BUF_SIZE);
    int i;
    for(i=2;i<n;i++)
      msg2[i-2]=msg[i];
    parse(msg2);
  }
}*/
/* FIN Thread */





/* Principale */

// Initialisation du programme
// Connexion au serveur
void debut() {
    sock = init_connection();
    if(spectator && mdp != NULL)
        pspectator(sock, pseudo, mdp);
    else if (spectator)
        pspectator(sock, pseudo, NULL);
    else if(newUser && mdp != NULL) {
        // Enregistrement
        printf("Enregistrement\n");
        pregister(sock, pseudo, mdp);
    } else if (mdp != NULL) {
        // Login
        printf("Login\n");
        plogin(sock, pseudo, mdp);
    } else {
        // Connect
        printf("Connexion\n");
        pconnect(sock, pseudo);
    }

    // Ancien thread avant passage a qt
    //pthread_create(&thread_lecture, NULL, lecture, (void*)sock);
}


// Fin du programme
// Fermeture de la socket
void finir(){
    printf("Exit\n");
    pexit(sock, pseudo);

    end_connection(sock);
}
/* FIN Principale */



