#include "client.h"

/* String */
const char *my_itoa_buf(char *buf, size_t len, int num){
    static char loc_buf[sizeof(int) * 60]; /* not thread safe */

    if (!buf){
        buf = loc_buf;
        len = sizeof(loc_buf);
    }

    if (snprintf(buf, len, "%d", num) == -1)
        return ""; /* or whatever */

    return buf;
}

const char *my_itoa(int num){ 
    return my_itoa_buf(NULL, 0, num);
}

/* FIN String */
