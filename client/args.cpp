#include "client.h"
#include "variable.h"


char *pseudo;
char *mdp;
int newUser;
int spectator;
int dessinateur;

SOCKET sock;
int port;
char *host;


pthread_t thread_lecture;


void gestionArg(int argc, char* argv[]) {
    dessinateur = 0;
    spectator = 0;
    newUser = 0;
    pseudo = (char*) malloc(sizeof(char)*BUF_SIZE);
    pseudo = "pc2r";
    mdp = NULL;
    port = PORT;
    host = (char*) malloc(sizeof(char)*BUF_SIZE);
    host = ADDRESS;

    int i;
    for(i=2;i<=argc;i=i+2) {
        if(strcmp(argv[i-1],"-host")== 0) {
            host = argv[i];
        }
        else if(strcmp(argv[i-1],"-port")== 0)
            port = atoi(argv[i]);
        else if(strcmp(argv[i-1],"-user")== 0)
            pseudo = argv[i];
        else if(strcmp(argv[i-1],"-newUser")== 0) {
            newUser = 1;
            pseudo = argv[i];
        }
        else if(strcmp(argv[i-1],"-spectator")== 0) {
            spectator = 1;
            pseudo = argv[i];
        }
        else if(strcmp(argv[i-1],"-mdp")== 0) {
            mdp = (char*) malloc(sizeof(char)*BUF_SIZE);
            mdp = argv[i];
        }
    }


    /* debug arg
     printf("port :\t%d\n", port);
     printf("newUser :\t%d\n", newUser);
     printf("pseudo :\t%s\n", pseudo);
     printf("mdp :\t%s\n", mdp);
  */
}

