#ifndef VAR_H
#define VAR_H

#include "client.h"


/* Variable globale */


// Serveur 
extern SOCKET sock;
extern int port;
extern char *host;



// Joueur
extern char *pseudo;
extern char *mdp;
extern int newUser;
extern int spectator;
extern int dessinateur;



// Autre
extern pthread_t thread_lecture;
/* FIN var globale */


#endif /* guard */
