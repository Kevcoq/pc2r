#include "client.h"
#include "parse.h"
#include "socket.h"
#include "variable.h"
#include "str.h"
#include "isketch.h"

#define BUF_SIZE 100

Parse::Parse() :
    QThread() {
}

Parse::~Parse()
{
    exit();
    wait();
}

void Parse::run()
{
    char *msg = (char*) malloc(sizeof(char)*BUF_SIZE);
    int n;


    while((n = read_server((SOCKET)sock, msg)))
        parse(msg);
}


/* Parser */
void Parse::parse(char *string) {
    char *buf;
    buf = strtok(string, "/");


    if(strcmp(buf, "ACCESSDENIED") == 0)
        emitAccessDenied();
    else if(strcmp(buf, "CONNECTED") == 0)
        parseConnected();
    else if(strcmp(buf, "EXITED") == 0)
        parseExited();
    else if(strcmp(buf, "NEW_ROUND") == 0)
        parseNewRound();
    else if(strcmp(buf, "GUESSED") == 0)
        parseGuessed();
    else if(strcmp(buf, "WORD_FOUND") == 0)
        parseWordFound();
    else if(strcmp(buf, "WORD_FOUND_TIMEOUT") == 0)
        parseWordFoundTimeout();
    else if(strcmp(buf, "END_ROUND") == 0)
        parseEndRound();
    else if(strcmp(buf, "SCORE_ROUND") == 0)
        parseScoreRound();
    else if(strcmp(buf, "LINE") == 0)
        parseLine();
    else if(strcmp(buf, "COURBE") == 0) {
        parseCourbe();
    }
    else if(strcmp(buf, "LISTEN") == 0)
        parseListen();
}
/* FIN Parser */


















/* Parse fct */

/* Connexion */
void Parse::parseConnected() {
    char *user;
    user = strtok(NULL, "/");

    printf("Connexion de %s.\n", user);
}

/* Exit */
void Parse::parseExited() {
    char *user;
    user = strtok(NULL, "/");

    printf("Deconnexion de %s.\n", user);
}
/* FIN Connexion */





















/* Tour */
void Parse::parseNewRound() {
    char *role;
    role = strtok(NULL, "/");

    if(strcmp(role, "dessinateur") == 0) {
        char *mot;
        mot = strtok(NULL, "/");

        char* tmp = (char*) malloc(sizeof(char)*BUF_SIZE);
        strcat(tmp, mot);

        emit emitRoleMot("Dessinateur", tmp);
        //printf("Role : Dessinateur\t mot : %s.\n", mot);


        dessinateur = 1;

    }
    else {
        dessinateur = 0;
        //printf("Role : %s.\n", role);
        emit emitRoleMot("Chercheur", "? mot ?");
    }
}



void Parse::parseGuessed() {
    char *mot;
    mot = strtok(NULL, "/");
    char *user;
    user = strtok(NULL, "/");

    char *tmp = (char*) malloc(sizeof(char) * BUF_SIZE);
    memset(tmp, 0, BUF_SIZE);

    strcat(tmp, user);
    strcat(tmp, " propose ");
    strcat(tmp, mot);
    strcat(tmp, ".");

    //printf("%s\n", tmp);
    emit emitGame(tmp);
}



void Parse::parseWordFound() {
    char *user;
    user = strtok(NULL, "/");

    char *tmp = (char*) malloc(sizeof(char) * BUF_SIZE);
    memset(tmp, 0, BUF_SIZE);


    strcat(tmp, user);
    strcat(tmp, " a trouve le mot.");

    //printf("%s\n", tmp);
    emit emitGame(tmp);
}



void Parse::parseWordFoundTimeout() {
    char *time;
    time = strtok(NULL, "/");

    char *tmp = (char*) malloc(sizeof(char) * BUF_SIZE);
    memset(tmp, 0, BUF_SIZE);

    strcat(tmp, "Temps restant ");
    strcat(tmp, time);
    strcat(tmp, " seconde.");

    //printf("%s\n", tmp);
    emit emitGame(tmp);
}



void Parse::parseEndRound() {
    char *user;
    user = strtok(NULL, "/");
    char *mot;
    mot = strtok(NULL, "/");

    char *tmp = (char*) malloc(sizeof(char) * BUF_SIZE);
    memset(tmp, 0, BUF_SIZE);

    if(strcmp(mot, "")) {
        strcat(tmp, "Tour fini, aucun gagnant, ");
        strcat(tmp, " mot : ");
        strcat(tmp, user);
    }
    else {
        strcat(tmp, "Tour fini, gagnant ");
        strcat(tmp, user);
        strcat(tmp, " avec ");
        strcat(tmp, mot);
    }

    //printf("%s.\n", tmp);
    dessinateur = 0;

    emit emitGame(tmp);
}



void Parse::parseScoreRound() {
    char *user;
    char* score;

    char *tmp = (char*) malloc(sizeof(char) * BUF_SIZE);
    memset(tmp, 0, BUF_SIZE);

    strcat(tmp, "Score : ");

    while((user = strtok(NULL, "/")) != NULL) {
        score = strtok(NULL, "/");

        if(score != NULL) {

            strcat(tmp, user);
            strcat(tmp, " -> ");
            strcat(tmp, score);
            strcat(tmp, " \t ");
        }
    }


    //printf("%s\n", tmp);

    emit emitGame(tmp);
}
/* FIN Tour */



















/* Dessin */
void Parse::parseLine() {
    int x1, y1, x2, y2, r, g, b, size;

    x1 = atoi(strtok(NULL, "/"));
    y1 = atoi(strtok(NULL, "/"));
    x2 = atoi(strtok(NULL, "/"));
    y2 = atoi(strtok(NULL, "/"));
    r = atoi(strtok(NULL, "/"));
    g = atoi(strtok(NULL, "/"));
    b = atoi(strtok(NULL, "/"));
    size = atoi(strtok(NULL, "/"));

    //printf("Dessiner la ligne de (%d;%d) a (%d;%d), taille %d, couleur (%d;%d;%d).\n", x1, y1, x2, y2, size, r, g, b);
    emit emitLine(x1, y1, x2, y2, r, g, b, size);

}


void Parse::parseCourbe() {
    int x1, y1, x2, y2, x3, y3, x4, y4, r, g, b, size;

    x1 = atoi(strtok(NULL, "/"));
    y1 = atoi(strtok(NULL, "/"));
    x2 = atoi(strtok(NULL, "/"));
    y2 = atoi(strtok(NULL, "/"));
    x3 = atoi(strtok(NULL, "/"));
    y3 = atoi(strtok(NULL, "/"));
    x4 = atoi(strtok(NULL, "/"));
    y4 = atoi(strtok(NULL, "/"));
    r = atoi(strtok(NULL, "/"));
    g = atoi(strtok(NULL, "/"));
    b = atoi(strtok(NULL, "/"));
    size = atoi(strtok(NULL, "/"));

    //printf("Courbe : (%d;%d) |  (%d;%d) |  (%d;%d) |  (%d;%d)\n", x1, y1, x2, y2, x3, y3, x4, y4);

    emit emitCourbe(x1, y1, x2, y2, x3, y3, x4, y4, r, g, b, size);
}

/* FIN Dessin */
























/* Chat */
void Parse::parseListen() {
    char *user;
    user = strtok(NULL, "/");
    char *txt;
    txt = strtok(NULL, "/");

    char *tmp = (char*) malloc(sizeof(char) * BUF_SIZE);
    memset(tmp, 0, BUF_SIZE);

    strcat(tmp, user);
    strcat(tmp, " : ");
    strcat(tmp, txt);
    strcat(tmp, ".");

    //printf("%s\n", tmp);    char *tmp = (char*) malloc(sizeof(char) * BUF_SIZE);
    emit emitChat(tmp);
}
/* FIN Chat */

/* FIN Parse fct */

