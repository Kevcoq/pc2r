#ifndef PARSE_H
#define PARSE_H

#include <QThread>
#include "isketch.h"


class Parse : public QThread
{
    Q_OBJECT
public:
    Parse();
    virtual ~Parse();

public slots:
    virtual void run();

signals:
    void emitAccessDenied();
    void emitGame(char *);
    void emitChat(char *);
    void emitRoleMot(char *, char *);
    void emitLine(int, int, int, int, int, int, int, int);
    void emitCourbe(int, int, int, int, int, int, int, int, int, int, int, int);


private :
    /* Parse */
    void parse(char *string);

    /* Fct parse */
    // Connexion
    void parseConnected();
    void parseExited();

    // Round
    void parseNewRound();
    void parseGuessed();
    void parseWordFound();
    void parseWordFoundTimeout();
    void parseEndRound();
    void parseScoreRound();

    // Dessin
    void parseLine();
    void parseCourbe();

    // Chat
    void parseListen();
};

#endif /* guard */
