#ifndef ISKTECH_H
#define ISKTECH_H

#include <QtGui>
#include <QWidget>
#include "ui_isketch.h"

namespace Ui {
class iSketch;
}

class iSketch : public QWidget, private Ui::iSktech
{
    Q_OBJECT
    
public:
    iSketch();

protected:
    // dessin (souris / paint)
    bool eventFilter(QObject *obj, QEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void paintEvent(QPaintEvent *);

private slots:
    void closeEvent(QCloseEvent *);
    void on_passButton_clicked();
    void on_cheatButton_clicked();
    void on_chatButton_accepted();
    void on_gameButton_accepted();
    void on_sizeSlider_valueChanged(int n);
    void on_colorButton_clicked();

    void accessdenied();
    void addGame(char*);
    void addChat(char*);
    void addRoleMot(char *, char *);
    void addLine(int, int, int, int, int, int, int, int);
    void addCourbe(int , int , int , int , int , int , int , int ,int , int , int , int);

private:
    quint16 tailleMessage;
    // points cliqués
    int nbPts;
    QPoint p1, p2, p3, p4;
    QVector<QLine> v_lines;
    QVector<QPainterPath> v_path;
    QVector<QPen> v_pen;
    QVector<QPen> v_pen_path;
};


#endif // ISKTECH_H
