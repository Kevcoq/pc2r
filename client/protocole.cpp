#include "client.h"
#include "variable.h"
#include "str.h"

/* Protocole */
// Connexion
void pconnect(SOCKET sock, const char *pseudo) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "CONNECT/");
    strcat(string, pseudo);
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Enregistrement
void pregister(SOCKET sock, const char *pseudo, const char *mdp) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "REGISTER/");
    strcat(string, pseudo);
    strcat(string, "/");
    strcat(string, mdp);
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Login
void plogin(SOCKET sock, const char *pseudo, const char *mdp) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "LOGIN/");
    strcat(string, pseudo);
    strcat(string, "/");
    strcat(string, mdp);
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Spectateur
void pspectator(SOCKET sock, const char *pseudo, const char *mdp) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "SPECTATOR/");
    strcat(string, pseudo);
    if(mdp != NULL) {
        strcat(string, "/");
        strcat(string, mdp);
    }
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Exit
void pexit(SOCKET sock, const char *pseudo) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "EXIT/");
    strcat(string, pseudo);
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Guess
void pguess(SOCKET sock, const char *mot) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "GUESS/");
    strcat(string, mot);
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Pass
void ppass(SOCKET sock) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "PASS/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Cheat
void pcheat(SOCKET sock, const char *user) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "CHEAT/");
    strcat(string, user);
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Color
void pcolor(SOCKET sock, const int r, const int g, const int b) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "SET_COLOR/");
    strcat(string, my_itoa(r));
    strcat(string, "/");
    strcat(string, my_itoa(g));
    strcat(string, "/");
    strcat(string, my_itoa(b));
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Size
void psize(SOCKET sock, const int size) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    strcat(string, "SET_SIZE/");
    strcat(string, my_itoa(size));
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Line
void pline(SOCKET sock, const int x1, const int y1, const int x2, const int y2) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "SET_LINE/");
    strcat(string, my_itoa(x1));
    strcat(string, "/");
    strcat(string, my_itoa(y1));
    strcat(string, "/");
    strcat(string, my_itoa(x2));
    strcat(string, "/");
    strcat(string, my_itoa(y2));
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Courbe
void pcourbe(SOCKET sock, const int x1, const int y1, const int x2, const int y2, const int x3, const int y3, const int x4, const int y4) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "SET_COURBE/");
    strcat(string, my_itoa(x1));
    strcat(string, "/");
    strcat(string, my_itoa(y1));
    strcat(string, "/");
    strcat(string, my_itoa(x2));
    strcat(string, "/");
    strcat(string, my_itoa(y2));
    strcat(string, "/");
    strcat(string, my_itoa(x3));
    strcat(string, "/");
    strcat(string, my_itoa(y3));
    strcat(string, "/");
    strcat(string, my_itoa(x4));
    strcat(string, "/");
    strcat(string, my_itoa(y4));
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

// Talk
void ptalk(SOCKET sock, const char *phrase) {
    char *string = (char*) malloc(sizeof(char)*(BUF_SIZE));
    memset(string, 0, sizeof(string));
    strcat(string, "TALK/");
    strcat(string, phrase);
    strcat(string, "/\n");
    if(send(sock, string, strlen(string), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}
/* FIN Protocole */
