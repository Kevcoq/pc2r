#ifndef PROTOCOLE_H
#define PROTOCOLE_H

#include "client.h"

/* Protocole */
void pconnect(SOCKET sock, const char *pseudo);
void pregister(SOCKET sock, const char *pseudo, const char *mdp);
void plogin(SOCKET sock, const char *pseudo, const char *mdp);
void pspectator(SOCKET sock, const char *pseudo, const char *mdp);
void pexit(SOCKET sock, const char *pseudo);
void pguess(SOCKET sock, const char *mot);
void ppass(SOCKET sock);
void pcheat(SOCKET sock, const char *user);
void pcolor(SOCKET sock, const int r, const int g, const int b);
void psize(SOCKET sock, const int size);
void pline(SOCKET sock, const int x1, const int y1, const int x2, const int y2);
void pcourbe(SOCKET sock, const int x1, const int y1, const int x2, const int y2, const int x3, const int y3, const int x4, const int y4);
void ptalk(SOCKET sock, const char *phrase);
/* FIN Protocole */



#endif /* guard */
